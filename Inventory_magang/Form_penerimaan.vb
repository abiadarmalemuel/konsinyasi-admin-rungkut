﻿Imports MySql.Data.MySqlClient
Imports DevExpress.XtraSplashScreen
Imports System.Globalization

Public Class Form_penerimaan
    Dim x As Integer
    Private Sub Form_retur_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim newCulture As CultureInfo = DirectCast(CultureInfo.CurrentCulture.Clone(), CultureInfo)
        newCulture.DateTimeFormat = (New CultureInfo("id-ID")).DateTimeFormat
        newCulture.NumberFormat = (New CultureInfo("id-ID")).NumberFormat
        System.Threading.Thread.CurrentThread.CurrentCulture = newCulture
        System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture

        Dim nn As Integer = dataquery("select * from mpenerimaan where month(createDate)=" & DateTime.Now.Month.ToString & " and year(createDate)=" & DateTime.Now.Year.ToString & "", conappkonsinyasi).Rows.Count
        If nn <> 0 Then
            x = Val(dataquery("select idNum from mpenerimaan where month(createDate)=" & DateTime.Now.Month.ToString & " and year(createDate)=" & DateTime.Now.Year.ToString & " order by id desc limit 1", conappkonsinyasi).Rows(0).Item("idNum").ToString)
            x = x + 1
        Else
            x = 1
        End If
        idlaritta.Text = "PK/" + DateTime.Now.Year.ToString().Substring(2, 2) + "/" + DateTime.Now.Month.ToString("#00") + "/" + getUserZeroId(x.ToString)



        Dim dt As DataTable = New DataTable
        dt = dataquery("select idSupplier 'ID',namaSupplier 'Supplier',Catatan, Telepon  from m_supplier where isKonsinyasi=1 ", conapppurchasing)
        LookUpEdit1.Properties.DataSource = dt
        LookUpEdit1.Properties.DisplayMember = "Supplier"
        LookUpEdit1.Properties.ValueMember = "ID"
        LookUpEdit1.Properties.PopulateColumns()
        LookUpEdit1.Properties.Columns("ID").Visible = False





        RepositoryItemLookUpEdit1.DataSource = dataquery("select m.idBrg 'id',m.namaBrg 'Barang',nm.namaKategori2 'Kategori' from mbarang m,mkategoribrg2 nm, app_konsinyasi.mbarang_konsinyasi mk where m.idBrg =mk.idBrg and nm.idKategori2=m.idKategori2; ", condblaritta)
        RepositoryItemLookUpEdit1.ValueMember = "id"
        RepositoryItemLookUpEdit1.DisplayMember = "Barang"
        RepositoryItemLookUpEdit1.PopulateColumns()
        RepositoryItemLookUpEdit1.Columns("id").Visible = False






    End Sub

    Private Sub SimpleButton1_Click(sender As Object, e As EventArgs) Handles SimpleButton1.Click
        Dim simpan As Boolean = False
        If LookUpEdit1.EditValue <> Nothing And nofaktur.EditValue <> Nothing And DateEdit1.EditValue <> Nothing Then

            For index = 0 To GridView1.RowCount - 1

                If GridView1.GetRowCellValue(index, GridView1.Columns(0)).ToString <> "" And GridView1.GetRowCellValue(index, GridView1.Columns(2)).ToString <> "" Then
                    simpan = True

                End If
            Next

        End If
        If simpan = True Then

            SplashScreenManager.ShowForm(GetType(WaitForm1))

            'insert master penerimaan
            insertall("insert into mpenerimaan(idNum,noFaktur,idStaff,idSupplier,catatan,createDate,void,isRetur,isInvoice) values(" & Val(x) & ",'" & nofaktur.Text & "'," & idstaff & "," & LookUpEdit1.EditValue & ",'" & catatan.Text & "','" & CDate(DateEdit1.EditValue).ToString("yyyy-MM-dd") & "',0,0,0) ", conappkonsinyasi)

            'insert detail penerimaan
            Dim num As Integer = dataquery("select id from mpenerimaan order by id desc limit 1", conappkonsinyasi).Rows(0).Item("id").ToString
            Dim rw As Integer = GridView1.RowCount - 1

            For index = 0 To rw
                insertall("insert into dtpenerimaan values(" & num & "," & GridView1.GetRowCellValue(index, GridView1.Columns(0)) & "," & repCSpr(GridView1.GetRowCellValue(index, GridView1.Columns(2))) & ")", conappkonsinyasi)
            Next

            'sum price for journal
            Dim sumall As Decimal = dataquery("select d.id,idBarang ,harga, qty, sum(harga*qty) 'total'
            from mpenerimaan m,dtpenerimaan d,mbarang_konsinyasi b
            where m.id=d.id and d.idBarang=b.idBrg and m.id=" & num & " GROUP BY d.id;", conappkonsinyasi).Rows(0).Item("total")

            'insert ke journal 
            insertall("CALL insert_journal(now(),19," & num & ",0,'penerimaan konsinyasi',null," & idstaff & ",now(),'1//85//0//" & repCSpr(sumall) & "|2//476//1//" & repCSpr(sumall) & "')", conappfinance)

            SplashScreenManager.CloseForm()

            Dim rs As DialogResult = MessageBox.Show("Data Tersimpan", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information)

            'buat id penerimaan baru
            Dim nn As Integer = dataquery("select * from mpenerimaan where month(createDate)=" & DateTime.Now.Month.ToString & " and year(createDate)=" & DateTime.Now.Year.ToString & "", conappkonsinyasi).Rows.Count
            If nn <> 0 Then
                x = Val(dataquery("select idNum from mpenerimaan where month(createDate)=" & DateTime.Now.Month.ToString & " and year(createDate)=" & DateTime.Now.Year.ToString & " order by id desc limit 1", conappkonsinyasi).Rows(0).Item("idNum").ToString)
                x = x + 1
            Else
                x = 1
            End If
            idlaritta.Text = "PK/" + DateTime.Now.Year.ToString().Substring(2, 2) + "/" + DateTime.Now.Month.ToString("#00") + "/" + getUserZeroId(x.ToString)

            LookUpEdit1.EditValue = Nothing
            nofaktur.Text = ""
            catatan.Text = ""

            For index = 0 To GridView1.RowCount
                GridView1.DeleteRow(index)
            Next
        Else
            Dim x As DialogResult = MessageBox.Show("Data belum lengkap", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)

        End If

    End Sub


    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click
        DateEdit1.EditValue = Nothing
        LookUpEdit1.EditValue = Nothing
        nofaktur.Text = ""
        catatan.Text = ""
        For index = 0 To GridView1.RowCount - 1
            GridView1.DeleteRow(index)
        Next
    End Sub

    Private Sub RepositoryItemButtonEdit1_Click(sender As Object, e As EventArgs) Handles RepositoryItemButtonEdit1.Click
        GridView1.DeleteSelectedRows()
    End Sub


End Class