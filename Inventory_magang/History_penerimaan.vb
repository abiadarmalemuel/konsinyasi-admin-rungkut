﻿Imports System.Globalization
Imports DevExpress.XtraSplashScreen
Imports DevExpress.XtraReports
Public Class History_penerimaan
    Private Sub Button1_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub History_penerimaan_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim newCulture As CultureInfo = DirectCast(CultureInfo.CurrentCulture.Clone(), CultureInfo)
        newCulture.DateTimeFormat = (New CultureInfo("id-ID")).DateTimeFormat
        newCulture.NumberFormat = (New CultureInfo("id-ID")).NumberFormat
        System.Threading.Thread.CurrentThread.CurrentCulture = newCulture
        System.Threading.Thread.CurrentThread.CurrentUICulture = newCulture

        DateEdit1.EditValue = FirstDayOfMonth(DateEdit1.DateTime)
        DateEdit2.EditValue = Now.Date

        gridhistory.DataSource = queryhistory(CheckEdit1.Checked)
        GridViewhistory.PopulateColumns()
        GridViewhistory.Columns("Penerimaan Konsinyasi").OptionsColumn.AllowEdit = False
        GridViewhistory.Columns("Faktur Supplier").OptionsColumn.AllowEdit = False
        GridViewhistory.Columns("Supplier").OptionsColumn.AllowEdit = False
        GridViewhistory.Columns("Date").OptionsColumn.AllowEdit = False
        GridViewhistory.Columns("Retur").OptionsColumn.AllowEdit = False







    End Sub

    Private Sub gridhistory_Click(sender As Object, e As EventArgs) Handles gridhistory.Click
        'memunculkan detail penerimaan konsinyasi berupa barang  qty

        Dim dts As New DataTable

        dts = dataquery("Select  m.namaBrg 'Barang',d.qty 'Qty Diterima', r.qty 'Qty Retur'
        From db_laritta.mbarang m, dtpenerimaan d, dtretur r, mretur mr
        Where r.id = mr.id And r.idBarang = d.idBarang And mr.idPenerimaan = d.id And d.id=" & getIdofIdNum(GridViewhistory.GetRowCellValue(GridViewhistory.FocusedRowHandle, GridViewhistory.Columns(0))) & " 
        And d.idBarang=m.idBrg;", conappkonsinyasi)
        If dts.Rows.Count = Nothing Then
            dts = dataquery("Select  m.namaBrg 'Barang',d.qty 'Qty Diterima', 0 'Qty Retur' From db_laritta.mbarang m, dtpenerimaan d Where d.id =" & getIdofIdNum(GridViewhistory.GetRowCellValue(GridViewhistory.FocusedRowHandle, GridViewhistory.Columns(0))) & "  And d.idBarang=m.idBrg;", conappkonsinyasi)


        End If
        GridControl1.DataSource = dts

        GridViewdetail.Columns("Qty Retur").Visible = True
    End Sub

    Private Sub CheckEdit1_CheckedChanged(sender As Object, e As EventArgs) Handles CheckEdit1.CheckedChanged
        gridhistory.DataSource = queryhistory(CheckEdit1.Checked)
    End Sub

    Private Sub DateEdit1_EditValueChanged(sender As Object, e As EventArgs) Handles DateEdit1.EditValueChanged
        gridhistory.DataSource = queryhistory(CheckEdit1.Checked)
    End Sub

    Private Sub DateEdit2_EditValueChanged(sender As Object, e As EventArgs) Handles DateEdit2.EditValueChanged
        gridhistory.DataSource = queryhistory(CheckEdit1.Checked)
    End Sub

    Private Function queryhistory(invoice As Boolean) As DataTable
        Dim dt As DataTable
        If invoice = True Then
            dt = dataquery("select concat('PK/',substring(year(m.createDate),3,2),'/',DATE_FORMAT(m.createDate,'%m'),'/',if(m.idNum<10,concat('00',m.idNum),if(m.idNum<100,concat('0',m.idNum),m.idNum))) 'Penerimaan Konsinyasi',noFaktur 'Faktur Supplier',namaSupplier 'Supplier', (m.createDate) 'Date',m.isRetur 'Retur'  from app_konsinyasi.mpenerimaan m, m_supplier where DATE((m.createDate))>='" & CDate(DateEdit1.EditValue).ToString("yyyy-MM-dd") & "' and DATE((m.createDate))<='" & CDate(DateEdit2.EditValue).ToString("yyyy-MM-dd") & "' and  m.idSupplier=m_supplier.idSupplier and void=0; ", conapppurchasing)
        Else
            dt = dataquery("select concat('PK/',substring(year(m.createDate),3,2),'/',DATE_FORMAT(m.createDate,'%m'),'/',if(m.idNum<10,concat('00',m.idNum),if(m.idNum<100,concat('0',m.idNum),m.idNum))) 'Penerimaan Konsinyasi',noFaktur 'Faktur Supplier',namaSupplier 'Supplier', (m.createDate) 'Date',m.isRetur 'Retur'  from app_konsinyasi.mpenerimaan m, m_supplier where DATE((m.createDate))>='" & CDate(DateEdit1.EditValue).ToString("yyyy-MM-dd") & "' and DATE((m.createDate))<='" & CDate(DateEdit2.EditValue).ToString("yyyy-MM-dd") & "' and m.isRetur=0 and m.idSupplier=m_supplier.idSupplier and void=0; ", conapppurchasing)
        End If
        Return dt
    End Function

    Private Sub SimpleButton2_Click(sender As Object, e As EventArgs) Handles SimpleButton2.Click

        Dim x As New returReport
        For index = 0 To GridViewhistory.RowCount - 1
            If GridViewhistory.IsRowSelected(index) = True Then
                If GridViewhistory.GetRowCellValue(index, GridViewhistory.Columns(4)) = True Then
                    x.Parameters("penkonsi").Value = getIdofIdNum(GridViewhistory.GetRowCellValue(index, GridViewhistory.Columns(0)))
                    x.Parameters("penerimaan").Value = "RK/" + (GridViewhistory.GetRowCellValue(index, GridViewhistory.Columns(0)).ToString).Substring(3, 9)
                    x.Parameters("Supplier").Value = GridViewhistory.GetRowCellValue(index, GridViewhistory.Columns(2))
                    x.Parameters("staff").Value = Module1.staff

                    Using prnt As UI.ReportPrintTool = New UI.ReportPrintTool(x)
                        SplashScreenManager.ShowForm(GetType(WaitForm1))
                        prnt.ShowPreviewDialog()
                        SplashScreenManager.CloseForm()
                    End Using
                Else
                    Dim j As DialogResult = MessageBox.Show("Tidak dapat cetak karena tidak ada data retur", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If



            End If


        Next

    End Sub





End Class